from ml4proflow.ml4proflow_cli import main

if __name__ == "__main__":
    main(["--graph-desc", "onnx_dfg.json", "--process-n-times", "1"])
