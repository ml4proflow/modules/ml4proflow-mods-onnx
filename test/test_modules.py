import unittest
from ml4proflow import modules
from ml4proflow_mods.onnx.modules import onnxModule

class TestModulesModule(unittest.TestCase):
    def setUp(self):
        self.dfm = modules.DataFlowManager()


    def test_create_onnxmodule(self):
        dut = onnxModule(self.dfm, {})
        self.assertIsInstance(dut, onnxModule)

    # define several tests


if __name__ == '__main__':
    unittest.main()
