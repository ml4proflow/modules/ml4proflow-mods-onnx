# ml4proflow-mods-onnx

This package provides modules to run ML models built in ONNX using onnxruntime with ml4proflow-

[![Tests Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/raw/tests-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/file/reports/junit/report.html?job=gen-cov)
[![Coverage Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/raw/coverage-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/file/reports/coverage/index.html?job=gen-cov)
[![Flake8 Status](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/raw/flake8-badge.svg?job=gen-cov)](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/file/reports/flake8/index.html?job=gen-cov)
[![mypy errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/raw/mypy.svg?job=gen-cov)]()
[![mypy strict errors](https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx/-/jobs/artifacts/master/raw/mypy_strict.svg?job=gen-cov)]()
------------



# Usage 
To configure the module, the following key/value pairs should be set: 
```json 
{
    "onnx_model" : ""
}
```
## Key/Value Description
- "onnx_model": Relative File Path to the .onnx-File 

## Installation
Activate your virtual environment (optionally) and
install the package with pip:
```bash 
pip install ml4proflow-mods-onnx
```

## Contribution
For development, install this repository in editable mode with pip:
```bash 
git clone https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx.git
cd ml4proflow-mods-onnx
pip install -e .
```
