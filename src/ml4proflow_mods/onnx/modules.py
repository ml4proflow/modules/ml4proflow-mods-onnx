from __future__ import annotations
from typing import Any, Union
from ml4proflow.modules import DataFlowManager, Module, SourceModule

import pandas
import onnxruntime as ort
import skl2onnx.common.data_types as cd



class onnxModule(Module):
    def __init__(self, dfm: DataFlowManager, config: dict[str, Any]):
        Module.__init__(self, dfm, config)
        # Load model & start session
        self.onnx_model = self.config.setdefault('onnx_model', "rf_iris.onnx")

    def on_new_data(self, name: str, sender: SourceModule,
                    data: pandas.DataFrame) -> None:

        self.sess = ort.InferenceSession(self.onnx_model)
        self.input_name = self.sess.get_inputs()[0].name
        self.label_name = self.sess.get_outputs()[0].name

        # Convert DataFrame to model specific data type
        dtype = cd.guess_numpy_type(cd.guess_data_type
                                    (self.sess.get_inputs()[0].type))
        X_data = data.to_numpy(dtype=dtype)

        # Get prediction
        pred_onx = self.sess.run([self.label_name],
                                 {self.input_name: X_data})[0]

        # Convert data type to DataFrame & push to channel
        df = pandas.DataFrame(pred_onx, columns=[self.label_name])
        #print(df)

        for channel in self.config["channels_push"]:
            self._push_data(channel, df)

    @classmethod
    def get_module_desc(cls) -> dict[str, Union[str, list[str]]]:
        return {"name": "ONNX Runtime",
                "categories": ["Inferenz"],
                "jupyter-gui-cls": "ml4proflow_jupyter.widgets.BasicWidget",
                "jupyter-gui-override-settings-type": {"onnx_model": "file"},
                "html-description": """
                Führt ein ONNX-Model aus.
                """
                }
