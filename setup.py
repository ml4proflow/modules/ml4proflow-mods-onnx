from setuptools import setup, find_namespace_packages

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

name = "ml4proflow-mods-onnx"
version = "0.0.1" 

cmdclass = {}

try:
    from sphinx.setup_command import BuildDoc
    cmdclass['build_sphinx'] = BuildDoc
except ImportError:
    print('WARNING: Sphinx not available, not building docs')

setup(
    name=name,
    version=version,
    author="Dennis Quirin",
    author_email="dquirin@techfak.uni-bielefeld.de",
    description="ML4ProFlow Module to run .onnx-Files",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.ub.uni-bielefeld.de/ml4proflow/modules/ml4proflow-mods-onnx",
    project_urls={
        "Main framework": "https://gitlab.ub.uni-bielefeld.de/ml4proflow/ml4proflow",
    },
    classifiers=[
        "Development Status :: 4 - Beta",
        'Intended Audience :: Developers',
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Artificial Intelligence",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],

    packages=find_namespace_packages(where="src"),
    namespace_packages=['ml4proflow_mods'],
    package_dir={"": "src"},
    data_files=[('ml4proflow/gui', ['data/rf_iris.onnx'])], # hack for GUI
    entry_points={
    },
    cmdclass=cmdclass,
    python_requires=">=3.6",
    install_requires=[
        "ml4proflow",
        "pandas",
        "onnxruntime",
        "numpy",
        "skl2onnx"
    ],
    extras_require={
        "tests": ["pytest", 
                  "pytest-html", 
                  "pytest-cov",
                  "pandas-stubs",
                  "mypy",
                  'jinja2>=3.1.0',
                  'pygments>=2.2.0',
                  'flake8>=3.3.0,<5.0.0',
                  'importlib-metadata;python_version<"3.8"'],
        "docs": ["sphinx",
                 "sphinx-rtd-theme",
                 "m2r2"],
    },
    command_options={
        'build_sphinx': {
            'project': ('setup.py', name),
            'version': ('setup.py', version),
            'release': ('setup.py', version),
            'source_dir': ('setup.py', 'docs/source/'),
            'build_dir': ('setup.py', 'docs/build/')
        }
    },
)
